polytopiary -- interaction in more dimensions
Copyright (C) 2006, 2007 Claude Heiland-Allen


This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


------------------------------------------------------------------------

Abstract data type.

------------------------------------------------------------------------

> module Grid (
>   Grid,
>   toGrid,
>   toList,
>   dims,
>   dim,
>   redim,
>   nest,
>   unnest,
>   join,
>   Grid.concat,
>   Grid.map,
>   Grid.zipWith,
>   for,
>   remap,
>   perspective,
>   outer,
>   inner,
>   innerl,
>   innerr,
>   rotation,

 >   putGridPPM8Bit,

> ) where

------------------------------------------------------------------------

>  import Array

 >  import IO
 >  import MissingH.IO.HVIO
 >  import MissingH.IO.Binary


------------------------------------------------------------------------

Concrete data type.

------------------------------------------------------------------------

>  data Grid a = G [Int] (Array Int a)
>    deriving (Show,Read,Eq)


------------------------------------------------------------------------

Grids are kinda numeric.

------------------------------------------------------------------------

>  instance Num a => Num (Grid a)
>    where
>      fromInteger x  = toGrid [] [fromInteger x]
>      negate         = Grid.map negate
>      (+)            = Grid.zipWith (+)
>      (*)            = Grid.zipWith (*)
>      abs            = Grid.map abs
>      signum         = Grid.map signum

>  instance Fractional a => Fractional (Grid a)
>    where
>      fromRational x = toGrid [] [fromRational x]
>      recip          = Grid.map recip
>      (/)            = Grid.zipWith (/)

>  instance Ord a => Ord (Grid a)
>    where
>      compare x y    = compare (toList x) (toList y)

>  instance Real a => Real (Grid a)
>    where
>      toRational (G d a)
>        | length d == 1 && head d == 1 = toRational (a!0)
>        | otherwise                    = error "Grid.toRational: size != 1"

>  instance Enum a => Enum (Grid a)
>    where
>      succ (G d a)
>        | length d == 1 && head d == 1 = toGrid [] [succ (a!0)]
>        | otherwise                    = error "Grid.succ: size != 1"
>      pred (G d a)
>        | length d == 1 && head d == 1 = toGrid [] [pred (a!0)]
>        | otherwise                    = error "Grid.pred: size != 1"
>      toEnum x                         = toGrid [] [toEnum x]
>      fromEnum (G d a)
>        | length d == 1 && head d == 1 = fromEnum (a!0)
>        | otherwise                    = error "Grid.fromEnum: size != 1"

>  instance Integral a => Integral (Grid a)
>    where
>      quot           = Grid.zipWith quot
>      rem            = Grid.zipWith rem
>      div            = Grid.zipWith div
>      mod            = Grid.zipWith mod
>      quotRem x y    = (Grid.zipWith quot x y, Grid.zipWith rem x y)
>      divMod  x y    = (Grid.zipWith div x y, Grid.zipWith mod x y)
>      toInteger (G d a)
>        | length d == 1 && head d == 1 = toInteger (a!0)
>        | otherwise                    = error "Grid.toInteger: size != 1"

>  instance Floating a => Floating (Grid a)
>    where
>      pi             = toGrid [] [pi]
>      exp            = Grid.map exp
>      log            = Grid.map log
>      sqrt           = Grid.map sqrt
>      (**)           = Grid.zipWith (**)
>      logBase        = Grid.zipWith logBase
>      sin            = Grid.map sin
>      cos            = Grid.map cos
>      tan            = Grid.map tan
>      asin           = Grid.map asin
>      acos           = Grid.map acos
>      atan           = Grid.map atan
>      sinh           = Grid.map sinh
>      cosh           = Grid.map cosh
>      tanh           = Grid.map tanh
>      asinh          = Grid.map asinh
>      acosh          = Grid.map acosh
>      atanh          = Grid.map atanh


-- this stuff errors due to "inferred type is not general enough"

  instance RealFrac a => RealFrac (Grid a)
    where
      ceiling        = Grid.map ceiling
      properFraction g = (Grid.map fst h, Grid.map snd h)
        where h = Grid.map properFraction g

------------------------------------------------------------------------

Validation.

------------------------------------------------------------------------

>  assert :: (a -> String) -> (a->Bool) -> a -> a
>  assert msg f x
>    | f x        = x
>    | otherwise  = error ("assert: " ++ (msg x))

>  assert_dim :: Int -> Int
>  assert_dim = assert (\x -> "dimension invalid: " ++ (show x)) (> 0)

>  assert_dims :: [Int] -> [Int]
>  assert_dims = filter (/= 0)

>  assert_bounds :: Int -> Array Int a -> Array Int a
>  assert_bounds n as = assert msg f as
>    where
>      msg x   = "array bounds corrupt: " ++ (show (bounds x))
>      f a     = g (bounds a)
>      g (0,m) = n == m+1

>  assert_grid :: Grid a -> Grid a
>  assert_grid (G ds as) = G ods oas
>    where
>      ods = assert_dims ds
>      oas = assert_bounds (foldr (*) 1 ds) as


------------------------------------------------------------------------

Abstract constructor.

>  toGrid :: [Int] -> [a] -> Grid a

------------------------------------------------------------------------

>  toGrid ds as = assert_grid (G dims (array (0,len-1) cells))
>    where
>      cells = zip [0..(len-1)] (Prelude.concat (repeat as))
>      len   = foldr (*) 1 dims
>      dims  = assert_dims ds


------------------------------------------------------------------------

Export as a list.

>  toList :: Grid a -> [a]

------------------------------------------------------------------------

>  toList (G _ as) = [ as!i | i <- range (bounds as) ]


------------------------------------------------------------------------

Number of dimensions.

>  dims :: Grid a -> Int

------------------------------------------------------------------------

>  dims (G d _) = length d

------------------------------------------------------------------------

Dimensions.

>  dim :: Grid a -> Grid Int

------------------------------------------------------------------------

>  dim (G [] _) = toGrid [] [0]
>  dim (G d  _) = toGrid [(length d)] d


------------------------------------------------------------------------

Redimension a grid.

>  redim :: [Int] -> Grid a -> Grid a

------------------------------------------------------------------------

>  redim ds g = toGrid ds (toList g)


------------------------------------------------------------------------

Apply a function to every cell.

>  map :: (a -> b) -> Grid a -> Grid b

------------------------------------------------------------------------

>  map f (G d x) = assert_grid (G d (array (bounds x)
>                      [(i, f (x!i)) | i <- range (bounds x)]))


------------------------------------------------------------------------

Combine two grids, redimensioning the second to match the first.

>  zipWith :: (a -> b -> c) -> Grid a -> Grid b -> Grid c

------------------------------------------------------------------------

>  zipWith f (G xd xa) (G yd ya) = toGrid xd (Prelude.zipWith f xl yl)
>    where
>      xl = toList (G xd xa)
>      yl = Prelude.concat (repeat (toList (G yd ya)))


------------------------------------------------------------------------

Nest grids at a given dimension index.

>  nest :: Int -> Grid a -> Grid (Grid a)

------------------------------------------------------------------------

>  nest n (G ds as) = toGrid od gs
>    where
>      gs = [ toGrid id [ as!i | i <- [r .. r+si-1]]
>           | r <- [0, si .. s-1] ]
>      si = foldr (*) 1 id
>      s  = foldr (*) 1 ds
>      od = take m ds
>      id = drop m ds
>      m  = assert (\x -> "cannot nest: " ++ (show x))
>                  (\x -> (0 <= x) && (x <= (length ds)))
>                  n


------------------------------------------------------------------------

Un-nest nested grids.

>  unnest :: Grid (Grid a) -> Grid a

------------------------------------------------------------------------

>  unnest (G ind ina) = toGrid outd outl
>    where
>      outd = ind ++ toList (dim (ina!0))
>      outl = Prelude.concat (toList (Grid.map toList (G ind ina)))


------------------------------------------------------------------------

Find coordinate corresponding to a subgrid.

>  coord :: [Int] -> [Int] -> Int

------------------------------------------------------------------------

>  coord ds xs = Prelude.foldr (+) 0 (Prelude.zipWith (*)
>                 (Prelude.drop 1 (Prelude.scanr (*) 1 ds))
>                 (xs ++ (repeat 0)))


------------------------------------------------------------------------

Reindex a grid.

>  reindex :: Grid Int -> Grid a -> Grid a

------------------------------------------------------------------------

>  reindex (G ld la) (G rd ra) = toGrid outd outl
>    where
>      outl     = [ ra!(coord rd [wa!i | i <- [r .. r+m-1]])
>                 | r <- [0, m .. s-1 ] ]
>      (G _ wa) = Grid.zipWith mod (G ld la) (dim (G rd ra))
>      s        = foldr (*) 1 ld
>      outd     = (Prelude.init ld) ++ subd
>      subd     = Prelude.drop m rd
>      m        = assert (\x -> "cannot reindex: " ++ (show x))
>                        (\x -> (0 < x) && (x <= (length rd)))
>                        (last ld)


------------------------------------------------------------------------

Join two grids at a given dimension.

>  join :: Int -> Grid a -> Grid a -> Grid a

------------------------------------------------------------------------

>  join 0 (G ld la) (G rd ra)
>    | tail ld == tail rd  = toGrid ([ head ld + head rd ]++tail ld)
>                                (toList (G ld la) ++ toList (G rd ra))
>    | otherwise           = error "Grid.join: mismatched tail dimensions"

>  join n (G ld la) (G rd ra)
>    | take n ld == take n rd = unnest (Grid.zipWith (join 0)
>                                (nest n (G ld la)) (nest n (G rd ra)))
>    | otherwise           = error "Grid.join: mismatched init dimensions"


------------------------------------------------------------------------

Join a list of grids at a given dimension.

>  concat :: Int -> [Grid a] -> Grid a

------------------------------------------------------------------------

>  concat n = foldr1 (join n)


------------------------------------------------------------------------

Generate a grid of coordinates.

>  for :: Grid a -> Grid Int -> Grid (a->a) -> Grid a

------------------------------------------------------------------------

>  lfor :: Int -> [a] -> [Int] -> [a->a] -> [[a]]
>  lfor d f c s
>    | d == 1   = take (head c) l
>    | d >  1   = Prelude.concat (Prelude.zipWith op
>                   (lfor 1 [head f] [head c] [head s])
>                   (repeat (lfor (d-1) (tail f) (tail c) (tail s))))
>      where
>        op [x] y = Prelude.map (x:) y
>        l = [head f] : Prelude.map (Prelude.map (head s)) l


>  for (G fromd froma) (G countd counta) (G stepd stepa)
>   | fromd == countd && countd == stepd && (length fromd) == 1 =
>       toGrid ((toList (G countd counta))++[head fromd]) (Prelude.concat
>                       (lfor
>                         (head fromd)
>                         (toList (G fromd froma))
>                         (toList (G countd counta))
>                         (toList (G stepd stepa))))
>   | otherwise = error "for: input size mismatched"


------------------------------------------------------------------------

Remap a grid.

>  remap :: (Grid Int -> Grid Int) -> Grid a -> Grid a

------------------------------------------------------------------------

>  remap f g = reindex (f c) g
>    where
>      c = for (toGrid [dims g] [0])
>              (toGrid [dims g] (toList (dim g)))
>              (toGrid [dims g] [(+1)])


------------------------------------------------------------------------

Perspective projection.

>  perspective :: (Num a, Fractional a) => Grid a -> Grid a -> Grid a

------------------------------------------------------------------------

>  perspective g z
>    | (last ds) > 1  = unnest $ Grid.zipWith o (nest ((length ds) -1) g) z
>    | otherwise      = error "Grid.perspective: last dimension too small"
>      where
>        o (G [d] x) q = toGrid [d-1]
>                        [(x!i) / ((x!(d-1) / q) + 1)| i <- [0..d-2]]
>        G ds _        = g


------------------------------------------------------------------------

Outer product.

>  outer :: (a -> b -> c) -> Grid a -> Grid b -> Grid c

------------------------------------------------------------------------

>  outer o (G ld la) (G rd ra) = G od oa
>    where
>      oa = array ob [ (i*rs+j, o (la!i) (ra!j)) | i <- is, j <- js ]
>      ob = (0, os - 1)
>      os = foldr (*) 1 od
>      od = ld ++ rd
>      rs = foldr (*) 1 rd
>      is = range (bounds la)
>      js = range (bounds ra)


------------------------------------------------------------------------

Fold.

  fold :: (b -> a -> b) -> b -> Grid a -> Grid b

------------------------------------------------------------------------

  fold f s x = o x
    where
      o gs         = foldl f s (toList gs)


------------------------------------------------------------------------

Inner product with arbitrary fold.

>  inner :: (a -> b -> c) -> ([c] -> d) -> Grid a -> Grid b -> Grid d

------------------------------------------------------------------------

>  inner o fold l r
>    | (last ld) == (head rd)  = G od (array ob oa)
>    | otherwise               = error "Grid.inner: mismatched dimensions"
>      where
>        oa     = [(oistep * i + okstep * k,
>                   fold [ o
>                     (la!(listep * i + ljstep * j))
>                     (ra!(rjstep * j + rkstep * k))
>                   | j <- [0..jmax]
>                   ])
>                 | i <- [0..limax]
>                 , k <- [0..rkmax]
>                 ]
>        ob     = (0,omax)
>        ld     = toList (dim l)
>        rd     = toList (dim r)
>        G _ la = l
>        G _ ra = r
>        od     = (init ld) ++ (tail rd)
>        omax   = (foldr (*) 1 od) - 1
>        oistep = (foldr (*) 1 (tail rd))
>        okstep = 1
>        limax  = (foldr (*) 1 (init ld)) - 1
>        listep = last ld
>        ljstep = 1
>        jmax   = (head rd) - 1 
>        rjstep = foldr (*) 1 (tail rd)
>        rkmax  = (foldr (*) 1 (tail rd)) - 1
>        rkstep = 1

------------------------------------------------------------------------

Inner product with standard folds.

>  innerl :: (a -> b -> c) -> (d -> c -> d) -> d -> Grid a -> Grid b -> Grid d
>  innerr :: (a -> b -> c) -> (c -> d -> d) -> d -> Grid a -> Grid b -> Grid d

------------------------------------------------------------------------

>  innerl o f s l r = inner o (foldl f s) l r
>  innerr o f s l r = inner o (foldr f s) l r


------------------------------------------------------------------------

Rotation about all axes.

>  rotation :: (Floating a) => Int -> [a] -> Grid a

------------------------------------------------------------------------

>  rotation d as
>    | length as == (d * (d-1)) `div` 2  = foldr (Grid.innerr (*) (+) 0)
>                                              (toGrid [d,d] ([1] ++ take d (repeat 0)))
>                                              rots
>    | otherwise = error "Grid.rotation: number of angles doesn't fit dimension"
>      where
>        rots = [ rot a i j
>               | (a,(i,j)) <- zip as [(i,j)
>               | i <- [0..(d-1)], j <- [i..(d-1)], i /= j ]
>               ]
>        rot a i j = Grid.map (r a i j) xy
>        xy = Grid.map toList (Grid.nest 2
>              (Grid.for (toGrid [2] [0]) (toGrid [2] [d]) (toGrid [2] [(+1)])))
>        r a i j [x,y]
>          | x == i && y == i  = cos a
>          | x == i && y == j  = sin a
>          | x == j && y == i  = - (sin a)
>          | x == j && y == j  = cos a
>          | x == y && x /= i && x/= j  = 1
>          | otherwise                  = 0


  static Matrix<Number> rotation(unsigned int d, std::vector<Number> as) {
    assert(d > 1);
    Matrix<Number> r(Matrix<Number>::identity(d));
    typename std::vector<Number>::iterator ai = as.begin();
    for (unsigned int i = 0; i < d; ++i) {
        for (unsigned int j = i + 1; j < d; ++j) {
            Matrix<Number> m(d, d);
            Number a = (*ai); ++ai;
            for (unsigned int x = 0; x < d; ++x) {
                for (unsigned int y = 0; y < d; ++y) {
                     if      (x == i && y == i) { m(x, y) =  cos(a); }
                     else if (x == i && y == j) { m(x, y) =  sin(a); }
                     else if (x == j && y == i) { m(x, y) = -sin(a); }
                     else if (x == j && y == j) { m(x, y) =  cos(a); }
                     else if (x == y)           { m(x, y) = 1; }
                     else                       { m(x, y) = 0; }
                }
            }
            r = r * m;
        }
    }
    return r;
  }

------------------------------------------------------------------------

PPM image output.

   1. A "magic number" for identifying the file type. A ppm image's magic number is the two characters "P6".
   2. Whitespace (blanks, TABs, CRs, LFs).
   3. A width, formatted as ASCII characters in decimal.
   4. Whitespace.
   5. A height, again in ASCII decimal.
   6. Whitespace.
   7. The maximum color value (Maxval), again in ASCII decimal. Must be less than 65536 and more than zero.
   8. Newline or other single whitespace character.
   9. A raster of Height rows, in order from top to bottom. Each row consists of Width pixels, in order from left to right. Each pixel is a triplet of red, green, and blue samples, in that order. Each sample is represented in pure binary by either 1 or 2 bytes. If the Maxval is less than 256, it is 1 byte. Otherwise, it is 2 bytes. The most significant byte is first.

      A row of an image is horizontal. A column is vertical. The pixels in the image are square and contiguous.
  10. In the raster, the sample values are "nonlinear." They are proportional to the intensity of the ITU-R Recommendation BT.709 red, green, and blue in the pixel, adjusted by the BT.709 gamma transfer function. (That transfer function specifies a gamma number of 2.2 and has a linear section for small intensities). A value of Maxval for all three samples represents CIE D65 white and the most intense color in the color universe of which the image is part (the color universe is all the colors in all images to which this image might be compared).

      ITU-R Recommendation BT.709 is a renaming of the former CCIR Recommendation 709. When CCIR was absorbed into its parent organization, the ITU, ca. 2000, the standard was renamed. This document once referred to the standard as CIE Rec. 709, but it isn't clear now that CIE ever sponsored such a standard.

      Note that another popular color space is the newer sRGB. A common variation on PPM is to subsitute this color space for the one specified.
  11. Note that a common variation on the PPM format is to have the sample values be "linear," i.e. as specified above except without the gamma adjustment. pnmgamma takes such a PPM variant as input and produces a true PPM as output.
  12. Characters from a "#" to the next end-of-line, before the maxval line, are comments and are ignored. 





  putGrid :: Handle -> (Handle -> a -> IO ()) -> Grid a -> IO [()]

------------------------------------------------------------------------

  putGrid handle putter (G [h,w,3] a) = do

     PPM header

    hPutStr  handle "P6 "
    hPutStr  handle (show w)
    hPutStr  handle " "
    hPutStr  handle (show h)
    hPutStr  handle " "
    hPutStr  handle (show 255)
    hPutChar handle '\n'

     pixel data

    mapM (putter handle) [ a!i | i <- range (bounds a) ]

------------------------------------------------------------------------

 >  putGridPPM8Bit :: (HVIO h) => h -> Grid Char -> IO ()

------------------------------------------------------------------------

 >  putGridPPM8Bit handle (G [h,w,3] a) = do

     PPM header

 >    vPutStr handle "P6\n# generated with gridskell\n"
 >    vPutStr handle (show w)
 >    vPutStr handle " "
 >    vPutStr handle (show h)
 >    vPutStr handle " "
 >    vPutStr handle (show 255)
 >    vPutStr handle "\n"

     pixel data

 >    vPutStr  handle [ a!i | i <- range (bounds a) ]


------------------------------------------------------------------------
-- EOF
