-- d01234 -- d0->d1->d2->d3->d4
-- Copyright (C) 2007 Claude Heiland-Allen
-- 
-- 
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License
-- as published by the Free Software Foundation; either version 2
-- of the License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


module Hyperspace where

import Data.IORef
import PureData
import Grid

import Foreign.C.Types (CFloat)
import Foreign.Ptr (Ptr)

data HState = HState {
  hColour    :: Ptr Symbol,
  hTranslate :: Ptr Symbol,
  hRecalc    :: Ptr Symbol,
  hBang      :: Ptr Symbol,
  hCube      :: Grid CFloat,
  hAngles    :: (CFloat,CFloat,CFloat,CFloat,CFloat,CFloat),
  hScales    :: (CFloat,CFloat,CFloat,CFloat),
  hCoords    :: [[CFloat]]
}

buildHypercube :: Int -> Grid CFloat
buildHypercube n = (Grid.for (toGrid [4] [-1]) (toGrid [4] [n]) (toGrid [4] [step]) )
  where
    step :: CFloat -> CFloat
    step = (+) (2/(fromInteger (toInteger n) - 1))

creator :: Creator
creator [AFloat n] = do
  colour    <- gensym "colour"
  translate <- gensym "translate"
  recalc    <- gensym "recalc"
  bang      <- gensym "bang"
  stateref  <- newIORef HState{
    hColour    = colour,
    hTranslate = translate,
    hRecalc    = recalc,
    hBang      = bang,
    hCube      = buildHypercube (fromIntegral (round n)),
    hAngles    = (0, 0, 0, 0, 0, 0),
    hScales    = (1, 1, 1, 1),
    hCoords    = []
  }
  return (Just Instance{iInlets = 4, iOutlets = 1, iMethod = inlet stateref})

creator _ = do
  return Nothing


-- inlets: getnext, recalc, angles, scales

inlet stateref outlet 0 _ _ = do
  state <- readIORef stateref
  writeIORef stateref state{hCoords = tail (hCoords state)}
  outlet 0 (hTranslate state) (coords state)
  return ()
  where
    coords state = [AFloat x, AFloat y, AFloat z]
      where
       [x, y, z] = head (hCoords state)

inlet stateref outlet 1 _ _ = do
  state <- readIORef stateref
  writeIORef stateref state{hCoords = newcoords state}
  return ()
  where
    newcoords state = toList (Grid.map toList (Grid.nest 4 (Grid.perspective
        ((Grid.innerr (*) (+) (0) ((hCube state) * (toGrid [4] [s0,s1,s2,s3]))
         (Grid.rotation 4 [a01,a02,a03,a12,a13,a23])) +
         (toGrid [4] [0,0,0,4])) 2)))
      where
        (a01,a02,a12,a03,a13,a23) = hAngles state
        (s0,s1,s2,s3) = hScales state

inlet stateref outlet 2 _ [AFloat a01, AFloat a02, AFloat a03, AFloat a12, AFloat a13, AFloat a23] = do
  state <- readIORef stateref
  writeIORef stateref state{hAngles = (a01,a02,a12,a03,a13,a23)}
  return ()

inlet stateref outlet 3 _ [AFloat s0, AFloat s1, AFloat s2, AFloat s3] = do
  state <- readIORef stateref
  writeIORef stateref state{hScales = (s0,s1,s2,s3)}
  return ()

inlet stateref outlet 2 _ _ = do
  return ()

inlet stateref outlet 3 _ _ = do
  return ()
