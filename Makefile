HOFLAGS = -fexcess-precision -fvia-C -optc-O2 -optc-mfpmath=sse -optc-msse2 -optc-march=pentium-m

Grid.o: Grid.lhs
	ghc $(HOFLAGS) -o Grid.o -c Grid.lhs
