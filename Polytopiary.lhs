polytopiary -- interaction in more dimensions
Copyright (C) 2006, 2007 Claude Heiland-Allen


This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


> module Main where

>  import Graphics.Rendering.OpenGL
>  import Graphics.UI.GLUT
>  import Data.IORef
>  import Data.Char ( chr, toLower )
>  import Data.Word
>  import System.Exit ( exitWith, ExitCode(ExitSuccess) )
>  import Foreign.Ptr
>  import Foreign.Marshal.Array
>  import IO
>  import System.IO ( openBinaryFile, hClose )
>  import MissingH.IO.Binary
>  import Foreign.C.Types
>  import Foreign.C.String
>  import Foreign.Marshal.Alloc

>  import Grid

>  data PState = PState {
>    pAngles   :: (GLfloat,GLfloat,GLfloat,GLfloat,GLfloat,GLfloat),
>    pSpeeds   :: (GLfloat,GLfloat,GLfloat,GLfloat,GLfloat,GLfloat),
>    pWidth    :: GLsizei,
>    pHeight   :: GLsizei,
>    pSnap     :: Int,
>    pPixels   :: Ptr CChar,
>    pHandle   :: PPMWriterHandle
>  }

>  buildHypercube :: Int -> Grid GLfloat
>  buildHypercube n = (Grid.for (toGrid [4] [-1]) (toGrid [4] [n]) (toGrid [4] [step]) )
>    where
>      step :: GLfloat -> GLfloat
>      step = (+) (2/(fromInteger (toInteger n) - 1))

>  hd = 4 :: Integer
>  hypercube = buildHypercube (fromInteger hd)

>  display stateref = do
>    state <- get stateref
>    let (a,b,c,d,e,f) = pAngles state
>    let s             = pSnap   state
>    let w             = pWidth  state
>    let h             = pHeight state
>    let p             = pPixels state
>    let handle        = pHandle state
>    clear [ColorBuffer, DepthBuffer ]
>    preservingMatrix $ do

>      scale 2 2 (2::GLfloat)
>      mapM_ (\([x,y,z],(cr,cg,cb)) -> preservingMatrix $ do

>          materialSpecular  Front $= Color4 cr cg cb 1
>          materialAmbient   Front $= Color4 cg cb cr 1
>          materialDiffuse   Front $= Color4 cb cr cg 1
>          materialShininess Front $= 50
>          translate $ Vector3 x y z
>          renderQuadric (QuadricStyle (Just Smooth)
>                         NoTextureCoordinates Outside
>                         FillStyle) (Sphere 0.03 12 12)) $
>        zip
>          (toList (Grid.map toList (Grid.nest 4 (Grid.perspective
>           ((Grid.innerr (*) (+) (0) hypercube 
>           (Grid.rotation 4 [a,b,c,d,e,f])) + (toGrid [4] [0,0,0,4])) 2))))
>          (Prelude.concat (repeat
>           [(fromInteger cr/fromInteger hd,
>             fromInteger cg/fromInteger hd,
>             fromInteger cb/fromInteger hd) | cr <- [1..hd], cg <- [1..hd], cb <- [1..hd]]))
>    swapBuffers

>  idle stateref = do
>    state <- get stateref
>    let ( a, b, c, d, e, f) = pAngles state
>    let (sa,sb,sc,sd,se,sf) = pSpeeds state
>    stateref $= state{pAngles =
>                (a + 2*pi*sa/3600, b + 2*pi*sb/3600, c + 2*pi*sc/3600,
>                 d + 2*pi*sd/3600, e + 2*pi*se/3600, f + 2*pi*sf/3600)}
>    postRedisplay Nothing
